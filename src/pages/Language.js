import '../App.css';
import Language from '../components/LanguageComponent';
import Header from '../components/Header';

function About() {
    const languageList = [
        {
            name: 'HTML & CSS',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
        },
        {
            name: 'JavaScript',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
        },
        {
            name: 'React',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
        },
        {
            name: 'Ruby',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
        },
        {
            name: 'Ruby on Rails',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
        },
        {
            name: 'Python',
            image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
        }
    ];

    return (
        <div className="App">
            <Header />
            {languageList.map((languageItem) => {
                return (
                    <Language
                        name={languageItem.name}
                        image={languageItem.image}
                    />
                )
            })}
        </div>
    );
}

export default About;
